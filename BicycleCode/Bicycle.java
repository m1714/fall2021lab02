//Nolan Ganz 2038533
package fall2021lab02.BicycleCode;

public class Bicycle {
    
    private String manufacturer;
    private int numberGears;
    private double maxSpeed;

    public Bicycle(String manufacturer, int numberGears, double maxSpeed)
    {
        this.manufacturer = manufacturer;
        this.numberGears = numberGears;
        this.maxSpeed = maxSpeed;
    }

    public String getManufacturer()
    {
        return this.manufacturer;
    }

    public int getNumberGears()
    {
        return this.numberGears;
    }

    public double getMaxSpeed()
    {
        return this.maxSpeed;
    }

    public String toString()
    {
        return "Manufacturer: " + this.manufacturer + ", Number of Gears: " + this.numberGears 
        + ", MaxSpeed: " + this.maxSpeed;
    }
}
